package eu.esic.formation.jsf.demo.managedbean;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

//@ManagedBean // Si pas de nom donné accessile avec le nom de classe en camelCase ex: personBeanManagedBean
// Depuis JFS 2.3 on a @Named a la place
@ManagedBean(name="personBean")
@SessionScoped
public class PersonBeanManagedBean implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private String name;
	
	public String getName() {
		return name; 
	}

	public void setName(String name) {
		this.name = name;
	}
}
